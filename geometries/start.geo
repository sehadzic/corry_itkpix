#MIMOSA26_0
[plane0]
#mask_file = "../output/MaskCreator/plane0/mask_plane0.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,0
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_1
[plane1]
#mask_file = "../output/MaskCreator/plane1/mask_plane1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,110mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_2
[plane2]
#mask_file = "../output/MaskCreator/plane2/mask_plane2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,219mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

#DUT0 Rd53a_110
#Change number_of_pixels depending on the sensor type: (800, 96) for 100x25 sensor; (400, 192) for 50x50 sensor; (200, 384) for 25x100 sensor.
#Chnage pixel_pitch depending on the sensor type: (25.0um,100.0um) for 100x25 sensor; (50um,50um) for 50x50 sensor; (100.0um,25.0um) for 25x100 sensor.
#Possible layouts are given in the form A_B_connectivity, where A is pixel pitch in the Y direction, B is pixel pitch in the X direction and connectivity to the readout chip as odd or even.
#Change spatial_resolution depending on the sensor type: (7.20um, 28.9um) for 100x25 sensor; (14.0um, 14.0um) for 50x50 sensor; (28.9um, 7.20um) for 25x100 sensor.
#Set correct value for the material budget taking into account thickness of the sensor and the readout chip. With the chip thickness of 400um and silicon radiation length of 93.65mm, the values for the material budget should be: 0.0053 for 100um sensor and 0.0059mm for 150um sensor.
[plane110]
#mask_file = "../output/MaskCreator/plane110/mask_plane110.txt"
material_budget = 0.003
number_of_pixels = 400, 192
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 50.0um,50.0um
position = 0,0,352.00mm
spatial_resolution = 14.4um, 14.4um
role = "dut"
time_resolution = 200ns
type = "Rd53a"

#DUT0 Rd53a_112
#Change number_of_pixels depending on the sensor type: (800, 96) for 100x25 sensor; (400, 192) for 50x50 sensor; (200, 384) for 25x100 sensor.
#Chnage pixel_pitch depending on the sensor type: (25.0um,100.0um) for 100x25 sensor; (50um,50um) for 50x50 sensor; (100.0um,25.0um) for 25x100 sensor.
#Possible layouts are given in the form A_B_connectivity, where A is pixel pitch in the Y direction, B is pixel pitch in the X direction and connectivity to the readout chip as odd or even
#Change spatial_resolution depending on the sensor type: (7.20um, 28.9um) for 100x25 sensor; (14.4um, 14.4um) for 50x50 sensor; (28.9um, 7.20um) for 25x100 sensor.
#Set correct value for the material budget taking into account thickness of the sensor and the readout chip. With the chip thickness of 400um and silicon radiation length of 93.65mm, the values for the material budget should be: 0.0053 for 100um sensor and 0.0059mm for 150um sensor.
[plane112]
#mask_file = "../output/MaskCreator/plane112/mask_plane112.txt"
material_budget = 0.003
number_of_pixels = 400, 192
orientation = 0deg, 0deg, 0deg
orientation_mod = "xyz"
pixel_pitch = 50.0um,50.0um
position = 0,0,422.00mm
spatial_resolution = 14.4um, 14.4um
time_resolution = 200ns
role = "dut"
type = "Rd53a"

#MIMOSA26_3
[plane3]
#mask_file = "../output/MaskCreator/plane3/mask_plane3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,568mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
role = "reference"
type = "mimosa26"

#MIMOSA26_4
[plane4]
#mask_file = "../output/MaskCreator/plane4/mask_plane4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,678mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

#MIMOSA26_5
[plane5]
#mask_file = "../output/MaskCreator/plane5/mask_plane5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0deg, 0deg, 0deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,789mm
spatial_resolution = 5.2um,5.2um
time_resolution = 230us
type = "mimosa26"

#reference plane FEI4
[plane21]
#mask_file = "../output/MaskCreator/plane21/mask_plane21.txt"
material_budget = 0.003
number_of_pixels = 80, 336
orientation = 180deg, 0deg, 90deg
orientation_mode = "xyz"
pixel_pitch = 250um,50um
position = 0,0,806mm
spatial_resolution = 72.0um, 14.0um
time_resolution = 25ns
role = "dut"
type = "FEI4"
