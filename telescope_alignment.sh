#!/bin/bash
HERE=$PWD
corry -c 03_align_telescope.conf -o number_of_tracks=10000 -o detectors_file="$HERE/geometries/prealignment.geo" -o detectors_file_updated="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=300um,300um -o AlignmentTrackChi2.max_track_chi2ndof=10 -o Tracking4D.min_hits_on_track=5
corry -c 03_align_telescope.conf -o number_of_tracks=2000 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=200um,200um -o  AlignmentTrackChi2.max_track_chi2ndof=10 -o Tracking4D.min_hits_on_track=5
corry -c 03_align_telescope.conf -o number_of_tracks=2000 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=100um,100um -o  AlignmentTrackChi2.max_track_chi2ndof=5 -o Tracking4D.min_hits_on_track=5
corry -c 03_align_telescope.conf -o number_of_tracks=2000 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=100um,100um -o  AlignmentTrackChi2.max_track_chi2ndof=5 -o Tracking4D.min_hits_on_track=6
corry -c 03_align_telescope.conf -o number_of_tracks=2000 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=50um,50um -o  AlignmentTrackChi2.max_track_chi2ndof=5 -o Tracking4D.min_hits_on_track=6
corry -c 03_align_telescope.conf -o number_of_tracks=2000 -o detectors_file="$HERE/geometries/alignment_tel.geo" -o Tracking4D.spatial_cut_abs=20um,20um -o AlignmentTrackChi2.max_track_chi2ndof=5 -o Tracking4D.min_hits_on_track=6
corry -c telescope_tracks.conf
